package arrayIndexList;

import java.util.Arrays;
import java.util.Iterator;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import indexList.IndexList;

public class ArrayIndexList<E> implements IndexList<E> {
	private static final int INITCAP = 1; 
	private static final int CAPTOAR = 1; 
	private static final int MAXEMPTYPOS = 2; 
	private E[] element; 
	private int size; 

	public ArrayIndexList() { 
		element = (E[]) new Object[INITCAP]; 
		size = 0; 
	} 
	

	public void add(int index, E e) throws IndexOutOfBoundsException {
		// ADD CODE AS REQUESTED BY EXERCISES
		if((index<0)||(index>this.size))
			throw new IndexOutOfBoundsException("add: invalid index = "+index);
		if(this.size==element.length)
			this.changeCapacity(CAPTOAR);
//		if(index==this.size){
//			this.add(e);
//			return;
//		}
		//para mover los elementos
/*		for(int i=size;i>index;i--)
			this.element[i]=this.element[i-1];*/
		moveDataOnePositionTR(index, size-1);
		
		this.element[index]=e;
		size++;
	}


	public void add(E e) {
		// ADD CODE AS REQUESTED BY EXERCISES
		if(this.size==element.length)
			this.changeCapacity(CAPTOAR);
		this.element[this.size++]=e;
		
	}


	public E get(int index) throws IndexOutOfBoundsException {
		// ADD AND MODIGY CODE AS REQUESTED BY EXERCISES
		if((index<0)||(index>=this.size))
			throw new IndexOutOfBoundsException("get: invalid index = "+index);
		
		return this.element[index]; 
	}


	public boolean isEmpty() {
		return size == 0;
	}


	public E remove(int index) throws IndexOutOfBoundsException {
		// ADD AND MODIFY CODE AS REQUESTED BY EXERCISES
		if((index<0)||(index>=this.size))
			throw new IndexOutOfBoundsException("remove: invalid index = "+index);
		
		E tmp=this.element[index];
		
//		for(int i=index;i<size;i++)
//			this.element[i]=this.element[i+1];
/*		if(size<=element.length/2)
			this.changeCapacity(-(element.length/2));*/
		moveDataOnePositionTL(index+1, size-1);
		if(MAXEMPTYPOS<=(element.length-size))
			this.changeCapacity(-CAPTOAR);
		//this.element[this.size]=null;
		size--;

		return tmp;
	}


	@Override
	public int capacity() {
		// TODO Auto-generated method stub
		return element.length;
	}


	public E set(int index, E e) throws IndexOutOfBoundsException {
		// ADD AND MODIFY CODE AS REQUESTED BY EXERCISES
		if((index<0)||(index>=this.size))
			throw new IndexOutOfBoundsException("set: invalid index = "+index);
		
		E tmp = this.element[index];
		this.element[index]=e;
		return tmp;
	}


	public int size() {
		return size;
	}	
	
	public void clear(){
		for(int i=0;i<this.size;i++)
			this.element[i]=null;
	}
	
	public Object clone(){
		E[] result = (E[]) new Object[this.element.length];
		return result;
	}
	
	public IndexList<E> subList(int fromIndex, int toIndex) 
			throws IndexOutOfBoundsException, IllegalArgumentException{
		if(fromIndex < 0 || toIndex > size)
			throw new IndexOutOfBoundsException();
		if(fromIndex>toIndex)
			throw new IllegalArgumentException();
		if(fromIndex == toIndex)
			return null;
		

		IndexList<E> result = new ArrayIndexList<>();
		
		
		return null;
	}
	
	
	// private methods  -- YOU CAN NOT MODIFY ANY OF THE FOLLOWING
	// ... ANALYZE AND USE WHEN NEEDED
	
	// you should be able to decide when and how to use
	// following method.... BUT NEED TO USE THEM WHENEVER
	// NEEDED ---- THIS WILL BE TAKEN INTO CONSIDERATION WHEN GRADING
	
	private void changeCapacity(int change) { 
		int newCapacity = element.length + change; 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = element[i]; 
			element[i] = null; 
		} 
		element = newElement; 
	}
	
	// useful when adding a new element with the add
	// with two parameters....
	private void moveDataOnePositionTR(int low, int sup) { 
		// pre: 0 <= low <= sup < (element.length - 1)
		for (int pos = sup; pos >= low; pos--)
			element[pos+1] = element[pos]; 
	}

	// useful when removing an element from the list...
	private void moveDataOnePositionTL(int low, int sup) { 
		// pre: 0 < low <= sup <= (element.length - 1)
		for (int pos = low; pos <= sup; pos++)
			element[pos-1] = element[pos]; 
	}


	// The following two methods are to be implemented as part of an exercise
	public Object[] toArray() {
		// TODO es in Exercise 3
		Object [] result = (E[]) new Object[this.size];
		for(int i=0;i<this.size;i++)
			result[i]=this.element[i];

		return result;
	}


	@Override
	public <T1> T1[] toArray(T1[] a) {
		// TODO as in Exercise 3
	    if (a.length < size)
	        // Make a new array of a's runtime type, but my contents:
	        return (T1[]) Arrays.copyOf(element, size, a.getClass());
	    System.arraycopy(element, 0, a, 0, size);
	    if (a.length > size)
	        a[size] = null;
	    return a;

	}

}
